/**
 * @author Hunar Karim
 * @email [hunar.karim@tib.eu]
 * @create date 2020-11-10
 * @modify date 2020-11-10
 * @desc [description]
 */
var text_1 = "Hier ist Ihr individueller Leitfaden zur Erstellung von offenen Bildungsmaterialien. In nur 5 Schritten sind Sie ein ganzes Stück näher an der Veröffentlichung Ihrer eigenen OER. Sollten sich im Erstellungsprozess doch noch Änderungen ergeben, dann prüfen Sie im Falle der Verwendung fremder Materialien bitte erneut mit Hilfe der OER-Planungshilfe die rechtlichen Bedingungen.";

var text_2 = "Bei der Erstellung von OER sollten Sie im Vorfeld überlegen, ob Sie Ihr Material komplett neu oder auf Grundlage bestehender Materialien erstellen möchten. Im Falle der Verwendung fremder Inhalte müssen Sie prüfen, ob Nutzungsrechte vorliegen.";

var text_3 = "Wählen Sie Programme oder Editoren, mit denen Sie vertraut sind. Es ist von Vorteil, wenn Sie möglichst freie oder weitverbreitete Programme und Tools nutzen, sodass Nutzer:innen ohne Kosten- und Zeitaufwand Zugang zu Ihrem Material erhalten und es nachnutzen und ggf. weiterbearbeiten können.";

var text_4 = "Konzipieren Sie das Material so übersichtlich, dass es lernfördernd und motivierend ist. Hier finden Sie einige Hinweise zur Gestaltung zu der von Ihnen gewählten Materialart.";

var text_5 = "Der Lizenzhinweis schafft die Grundlage für die Nutzung, ggf. Bearbeitung und Weiterverbreitung durch Nachnutzer:innen. Daher ist die vollständige Angabe des Lizenztextes am Material oder innerhalb des Materials von größter Bedeutung. Klare Lizenzhinweise schaffen Vertrauen und Sicherheit; sie fördern die Verwendung des Materials.";

var text_6 = "Stellen Sie Ihre Bildungsmaterialien in einem möglichst offenen Format bereit, indem Sie leicht versionierbare und editierbare Dateiformate beim Speichern wählen. So können Inhalte einfacher von anderen Lehrenden nachgenutzt werden. Sie nutzen (webbasierte) Lehr-/Lernumgebungen? Dann prüfen Sie, inwiefern Sie Ihre Inhalte öffentlich zugänglich machen können. Für Lernmanagementsysteme — insbesondere Ilias, Moodle und Stud.IP — kontaktieren Sie Ihre IT-Abteilung bzw. Ihr Rechenzentrum.";

var text_7 = "Hier finden Sie verschiedene Informationen und Angebote zum Thema „OER erstellen“.";
