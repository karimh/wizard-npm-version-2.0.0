/**
 * @author Hunar Karim
 * @email [hunar.karim@tib.eu]
 * @create date 2020-11-11
 * @modify date 2022-07-20
 * @desc [description]
 */
 var warning = "<div class='atention'>Das OER-Planungstool befindet sich aktuell im Aufbau. Leider werden noch keine Hinweise zu Lektion und Kurs angezeigt.";

var TOOLTIP_DATA=[
        

        {
                 // Format
            id:"b1_1",
            title: "<div class='tooltip1'>Visuelle Darstellung unterschiedlicher Abstraktion. Fotos, logische Bilder etc."
        },
        {
            id:"b1_2",
            title: "<div class='tooltip1'>Jegliche Artefakte oder Gegenstände, die Studierende in Lernhandlungen versetzen."
        },
        {
            id:"b1_3",
            title: "<div class='tooltip1'> Tondokumente, akustische Materialien"
        },
        {
            id:"b1_4",
            title: "<div class='tooltip1'>Durch spezifische Aktivitäten (meist methodisch geleitet) gewonnene Informationen über eine Sache bzw. einen Gegenstand"
        },
        {
            id:"b1_5",
            title: "<div class='tooltip1'>Visuelle Darstellung statistischer, nummerischer Daten; von Prozessen und Zusammenhängen"
        },
        {
            id:"b1_6",
            title: "<div class='tooltip1'>Material zur Realisation eines Experiments, Dokumentationen eines Experiments"
        },
        {
            id:"b1_7",
            title: "<div class='tooltip1'>Ein auf Kasuistik (≈ Einzelfallbetrachtung) basiertes methodisches Verfahren"
        },
        {
            id:"b1_8",
            title: "<div class='tooltip1'>Instrument der Datenerhebung bzw. Lernbegleitung"
        },
        {
            id:"b1_9",
            title: "<div class='tooltip1'>Visualisierungen topgrafischer, politischer Sachverhalte etc.; Methode zur Clusterung von Informationen"
        },
        {
            id:"b1_10",
            title: "<div class='tooltip1'>Eine in sich geschlossene Lehrveranstaltung, die aus mehreren Lehr-/Lerneinheiten bzw. Lektionen besteht"
        },
        {
            id:"b1_11",
            title: "<div class='tooltip1'>Umfassende Werke für Lehrende und Lernende zur strukturierten Vermittlung eines Themenkomplexes"
        },
        {
            id:"b1_12",
            title: "<div class='tooltip1'>Ein didaktisches Instrument, um den Wissenserwerb bzw. die Erreichung von Lernzielen zu überprüfen."
        },
        {
            id:"b1_13",
            title: "<div class='tooltip1'>Insbesondere Notationen; Auseinandersetzungen mit Musik und Noten"
        },
        {
            id:"b1_14",
            title: "<div class='tooltip1'>Ähnlich Lexika umfassende Werke zu bestimmten Themenkomplexen"
        },
        {
            id:"b1_15",
            title: "<div class='tooltip1'>Foliensätze zu bestimmten Themen, überfachliche Auseinandersetzung mit dem Präsentieren "
        },
        {
            id:"b1_16",
            title: "<div class='tooltip1'>Potentiell interaktive, immersive, illusorische Angebote (VR, Augmented Reality, Fulldome, Planspiel/Fallstudien); Dokumentationen von Simulationen"
        },
        {
            id:"b1_17",
            title: "<div class='tooltip1'>Verschriftliche Vorträge, die bei der Vermittlung und Erfassung von Lehr- und Lerninhalten unterstützen"
        },
        {
            id:"b1_18",
            title: "<div class='tooltip1'>Konkrete Aufgaben für einzelne Personen oder Gruppen (eventuell inklusive Kontrolle der Übung)"
        },
        {
            id:"b1_19",
            title: "<div class='tooltip1'>Schriftlich formulierte Überlegungen zur Lehrgestaltung"
        },
        {
            id:"b1_20",
            title: "<div class='tooltip1'>Audiovisuelle Medien — von Aufzeichnungen einer Vorlesung über Erklärvideos bis hin zu Demonstrationsvideos"
        },
        {
            id:"b1_21",
            title: "<div class='tooltip1'>Links zu konkreten Internetangeboten; ggf. Datensätze zu einer Website"
        },





        {
            //license
            id:"l1",
            title: "<div class='tooltip1'>Es werden alle Nutzungsrechte eingeräumt."
        },
        {
            id:"l2",
            title: "<div class='tooltip1'>Es werden alle Nutzungsrechte unter der Bedingung der Namensnennung der urhebenden Person eingeräumt."
        },
        {
            id:"l3",
            title: "<div class='tooltip1'>Es werden alle Nutzungsrechte unter Bedingungen der Namensnennung der urhebenden Person und der Weitergabe unter gleichen Bedingungen eingeräumt"
        },
        {
            id:"l4",
            title: "<div class='tooltip1'>Eingeschränkte Nutzungsrechte liegen vor: Das Werk darf nicht bearbeitet werden und die Namensnennung der urhebenden Person ist verpflichtend."
        },
        {
            id:"l5",
            title: "<div class='tooltip1'>Eingeschränkte Nutzungsrechte werden eingeräumt: Das Werk darf nicht kommerziell genutzt werden und die Namensnennung der urhebenden Person ist verpflichtend."
        },
        {
            id:"l6",
            title: "<div class='tooltip1'>Eingeschränkte Nutzungsrechte: Das Werk darf nicht kommerziell genutzt werden, die Namensnennung der urhebenden Person ist verpflichtend und die Weitergabe muss unter gleichen Bedingungen erfolgen."
        },
        {
            id:"l7",
            title: "<div class='tooltip1'>Stark eingeschränkte Nutzungsrechte: Die Namensnennung der urhebenden Person ist verpflichtend. Das Werk darf nicht kommerziell genutzt und nicht bearbeitet werden."
        },
        {
            id:"l8",
            title: "<div class='tooltip1'>Unter „Publikc Domain Mark“ sind gemeinfreie Werke gefasst. Es werden alle Nutzungsrechte eingeräumt. Prüfen Sie genau, ob es sich bei den fremden Materialien oder Inhalten um gemeinfreie Werke handelt."
        },
        {
            id:"l9",
            title: "<div class='tooltip1'>Bei urheberrechtlich geschützten Materialien sind alle Nutzungsrechte vorbehalten."
        },

        {
            //license 4.0 
            id:"l10",
            title: "<div class='tooltip1'>Sie erlauben eine uneingeschränkte Nutzung. Diese Lizenz ist im OER-Portal twillo ausdrücklich erwünscht."
        },
        {
            id:"l11",
            title: "<div class='tooltip1'>Sie erlauben eine uneingeschränkte Nutzung unter der Bedingung der Namensnennung. Diese Lizenz ist im OER-Portal twillo ausdrücklich erwünscht."
        },
        {
            id:"l12",
            title: "<div class='tooltip1'>Sie erlauben eine uneingeschränkte Nutzung unter der Bedingung der Namensnennung und Weitergabe unter gleichen Bedingungen. Diese Lizenz ist im OER-Portal twillo ausdrücklich erwünscht."
        },
        {
            id:"l13",
            title: "<div class='tooltip1'>Sie erlauben eine eingeschränkte Nutzung. Ihr Material darf zwar unter der Bedingung der Namensnennung geteilt, aber nicht bearbeitet werden. Mit dieser Lizenz könnten Nachteile für Nachnutzende entstehen."
        },
        {
            id:"l14",
            title: "<div class='tooltip1'>Sie erlauben eine eingeschränkte Nutzung unter der Bedingung der Namensnennung und nicht kommerzieller Nutzung. Mit dieser Lizenz könnten Nachteile für Nachnutzende entstehen."
        },
        {
            id:"l15",
            title: "<div class='tooltip1'>Sie erlauben eine eingeschränkte Nutzung unter der Bedingung der Namensnennung, nicht kommerziellen Nutzung und Weitergabe unter gleichen Bedingungen. Mit dieser Lizenz könnten Nachteile für Nachnutzende entstehen."
        },
	{
	            id:"l16",
            title: "<div class='tooltip1'>Sie erlauben eine eingeschränkte Nutzung unter der Bedingung der Namensnennung, nicht kommerziellen Nutzung und Ihr Material darf nicht bearbeitet werden. Mit dieser Lizenz könnten Nachteile für Nachnutzende entstehen."
        },
        {
            id:"q2",
            title: "<div class='tooltip1'>Einbinden heißt, ein genutztes Material Dritter bleibt im entstehenden Gesamtwerk separiert erhalten; es wird nicht mit anderen Inhalten untrennbar verschmolzen. Lizenzen genutzter Materialien und die des entstehenden Gesamtwerkes können Diskrepanzen aufweisen. Achtung: Einbindung in AV-Medien gilt stets als Bearbeitung."
        },
	    {
            id:"q1",
            title: "<div class='tooltip1'>Die Bearbeitung des Materials Dritter muss per Lizenz gestattet sein. Denn eine Bearbeitung kann vorliegen, wenn ein Bild beschnitten oder coloriert wird; weiterführend Inhalte ähnlich einer Collage miteinander verschmolzen werden und im entstehenden Gesamtwerk untrennbar sind."
        },
		{
            id:"q3",
            title: "<div class='tooltip1'>Sie müssen keine Lizenzen von Materialien Dritter beachten."
        },
    ]

