/**
 * @author Hunar Karim
 * @email [hunar.karim@tib.eu]
 * @create date 2020-11-10
 * @modify date 2022-07-20
 * @desc [description]
 */

function replay_step_5() {

    document.getElementById("step-4").style.display = "none";
    document.getElementById("step-5").innerHTML = "<center><img style='background-color: #FFFFFF;'"+
    "src='../image/wizard/image_7.png' width='300' height='300'>"+
   "<div style='margin-bottom: 70px; font-size: 28px;'>Gutes Gelingen bei der OER-Erstellung!</div></center>";

    //document.getElementById("pdfbutton").innerHTML = "";
	//document.getElementById("exportButton").innerHTML = "";
    document.getElementById("tab").setAttribute("style", "height: 100%");  
    document.getElementById("step-5").style.display = "block";
    pdf();
}